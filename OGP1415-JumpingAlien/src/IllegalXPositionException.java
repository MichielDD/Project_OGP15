import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of exceptions signalling illegal x-positions of the character Mazub. 
 * Each illegal x-position exception involves the Mazub character.
 * @author Steven Dobbelaere, Michiel De Deken
 *
 */
public class IllegalXPositionException extends RuntimeException {
	
	/**
	 * ######Analoog gedaan aan handboek pag 218-219######
	 * Initialize this new illegal x-position exception with a given xPosition and a given Mazub character
	 * @param 	xPosition
	 * 			| The x-position of this new illegal x-position exception.
	 * @param 	character
	 * 			| The Mazub character of this new illegal x-position exception.
	 * @post 	The x-position of this new illegal x-position exception is equal to the given position.
	 * 			| new.getXPosition() == xPosition
	 * @effect	This new illegal x-position exception is further initialized as a new runtime exception involving no 
	 * 			diagnostic message and no cause.
	 * 			| super() 
	 */
	public IllegalXPositionException(int xPosition, Mazub character){
		this.xPosition = xPosition;
		this.character = character;
	}
	
	/**
	 * Return the x-position of this illegal x-position exception.
	 */
	@Basic @Raw @Immutable
	public int getXPosition(){
		return xPosition;
	}
	
	/**
	 * Variable registering the x-position of this illegal x-position exception.
	 */
	private final int xPosition;
	
	/**
	 * Return the Mazub character involved in this illegal x-position exception.
	 */
	public Mazub getCharacter(){
		return character;
	}
	/**
	 * Variable referencing the Mazub character of this illegal x-position exception. 
	 */
	private final Mazub character;
	
	
	/**
	 * The Java API strongly recommends to explicitly define a version
	 * number for classes.
	 */
	private static final long serialVersionUID = -1426137869432404388L;

}
