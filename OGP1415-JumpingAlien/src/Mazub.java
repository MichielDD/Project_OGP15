import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * 
 * @author Steven Dobbelaere, Michiel De Deken
 * @version 1.0
 *
 */


public class Mazub {
	
	/**
	 * Initialize this new Mazub with given xPosition, given yPosition, given width and 
	 * given height.
	 * 
	 * @param   xPosition 
	 * 			The x coordinate of the bottom-left pixel of the Mazub character. 
	 * @param   yPosition	
	 * 			The y coordinate of the bottom-left pixel of the Mazub character. 			 
	 * @param   width		
	 * 			The width of the rectangle of the Mazub character.  			 
	 * @param   height	
	 * 			The height of the rectangle of the Mazub character. 			
//	 *@post   The bottom-left pixel of the Mazub is within the boundaries of the
//	 * 		   game world.
//	 *         | new.getX() <= getMax_X()
//	 *         | new.getX() >= getMin_X()
//	 * @post   The bottom-left pixel of the Mazub is within the boundaries of the
//	 * 		   game world.
//     *         | new.getY() <= getMax_Y()
//     *         | new.getY() >= getMin_Y()
     * @post   The xPosition of the new Mazub is equal to the given xPosition.
     *         | new.getX() == xPosition
     * @post   The yPosition of the new Mazub is equal to the given yPosition.
     *         | new.getY() == yPosition
     * @post   The width of the new Mazub is equal to the given width.
     *         | new.width() == width
     * @post   The height of the new Mazub is equal to the given height.
     *         | new.height() == height
	 * @throws IllegalxPositionException
	 * 		   The given xPosition is not a valid xPosition.
	 *         | ! isValidxPosition(xPosition)
	 * @throws IllegalyPositionException
	 * 		   The given yPosition is not a valid yPosition.
	 *         | ! isValidyPosition(yPosition)
	 * 
	 */
	public Mazub(int xPosition, int yPosition, int width, int height){ 
		//Waarschijnlijk nog 2 argumenten max en init velocity want beide moeten kunnen worden aangepast voor aparte Mazubs.
		setXPosition(xPosition);
		setYPosition(yPosition);
		setWidth(width);
		setHeight(height);
		
		
	}
	
	@Basic @Raw
	public int getHeight(){
		return this.height;
		
	}
	
	@Raw
	public void setHeight(int height){
		this.height = height;
	}
	
	public void setTest(int height){ //moet weg
		this.height = height;
	}
	/**
	 * Returning the current width of the Mazub character. 
	 * 
	 */
	@Basic @Raw
	public int getWidth(){
		return this.width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	/**
	 * Variables representing the range of horizontal pixels in the game world.
	 * The range of x positions is (0 , max_X).
	 * Each pixel is assumed to have a side length of 0.01 meters.
	 * 
	 */
	@Basic
	public int getMin_X() {
		return 0;
	}
	@Basic
	public int getMax_X() {
		// this is ((maximal number of pixels) - 1)
		return 1023;
	}
	
	/**
	 * Variables representing the range of vertical pixels in the game world.
	 * The range of y positions is (0 , max_Y).
	 * Each pixel is assumed to have a side length of 0.01 meters.
	 */
	@Basic
	public int getMin_Y() {
		return 0;
	}
	@Basic
	public int getMax_Y(){
		// this is ((maximal number of pixels) - 1)
		return 767;
	}
	
	/**
	 * Check whether the xPosition (of the bottom-left pixel of the Mazub) is within 
	 * the boundaries of the game world.
	 * 
	 * @param  xPosition
	 *         The xPosition to check.
	 * @return True if and only if the given xPosition is within the boundaries  
	 * 		   of the game world.
	 *         | result == (xPosition <= getMax_X() && xPosition >= getMin_X())
	 */
	public static boolean isValidXPosition(int xPosition) {		// moet hier wel static bij ? x positie verandert toch doorneen het spel ? 
		return (xPosition <= getMax_X() && xPosition >= getMin_X());
	}
	/**
	 * 
	 * @param xPosition
	 * @throws IllegalXPositionException
	 */
	public void setXPosition(int xPosition) throws IllegalXPositionException {
		if (! isValidXPosition(xPosition))
			throw new IllegalXPositionException(xPosition,this);
		this.xPosition = xPosition;
	}
	/**
	 * Check whether the yPosition (of the bottom-left pixel of the Mazub) is within 
	 * the boundaries of the game world.
	 * 
	 * @param  yPosition
	 *         The yPosition to check.
	 * @return True if and only if the given yPosition is within the boundaries of the
	 * 		   game world.
	 *         | result == (yPosition <= getMax_Y() && yPosition >= getMin_Y())
	 */
	public static boolean isValidYPosition(int yPosition) {
		return (yPosition <= getMax_Y() && yPosition >= getMin_Y());
	}
	/**
	 * 
	 * @param yPosition
	 * @throws IllegalYPositionException
	 */
	public void setYPosition(int yPosition) throws IllegalYPositionException {
		if (! isValidYPosition(yPosition))
			throw new IllegalYPositionException(yPosition,this);
		this.yPosition = yPosition;
	}	
	
	@Basic 	@Raw
	public double getInitialHorizontalVelocity(){
		return this.initialHorizontalVelocity;
	}
	
	public double getMaxHorizontalVelocity(){
		return this.maxHorizontalVelocity()
	}
	
	public double getHorizontalAcceleration(){
		return this.horizontalAcceleration()
		
	}
	
	public double acceleration = 0.9; 
	
	
	
	// opmerkingen/vragen:
	// bij @param staan in de voorbeelden precies nooit dingen als | new.getX() == x
	// moeten die dan weg?
	// Moeten we er nog expliciet voor zorgen dat de mazub deels het scherm mag verlaten
	// of gebeurt dit automatisch?
	// correct om die @posts te vervangen door @throw boven de public Mazub declaratie?
	// gaat java sws een fout geven als bvb height geen int is? (wrs wel)
	// nog @throw's bij public Mazub? (height en width niet groter dan game world ofzo?)
	//
	//
	// Op het einde nog eens heel de assignment doorlezen met de code ernaast.
}
