import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of exceptions signalling illegal y-positions of the character Mazub. 
 * Each illegal y-position exception involves the Mazub character.
 * @author Steven Dobbelaere, Michiel De Deken
 */
public class IllegalYPositionException extends RuntimeException {

	/**
	 * ######Analoog gedaan aan handboek pag 218-219######
	 * Initialize this new illegal y-position exception with a given xPosition and a given Mazub character
	 * @param 	yPosition
	 * 			| The y-position of this new illegal y-position exception.
	 * @param 	character
	 * 			| The Mazub character of this new illegal y-position exception.
	 * @post 	The y-position of this new illegal y-position exception is equal to the given position.
	 * 			| new.getYPosition() == yPosition
	 * @effect	This new illegal y-position exception is further initialized as a new runtime exception involving no 
	 * 			diagnostic message and no cause.
	 * 			| super() 
	 */
	public IllegalYPositionException(int yPosition, Mazub character){
		this.yPosition = yPosition;
		this.character = character;
	}
	
	/**
	 * Return the y-position of this illegal y-position exception.
	 */
	@Basic @Raw @Immutable
	public int getYPosition(){
		return yPosition;
	}
	
	/**
	 * Variable registering the y-position of this illegal y-position exception.
	 */
	private final int yPosition;
	
	/**
	 * Return the Mazub character involved in this illegal y-position exception.
	 */
	public Mazub getCharacter(){
		return character;
	}
	/**
	 * Variable referencing the Mazub character of this illegal y-position exception. 
	 */
	private final Mazub character;
	
	
	/**
	 * The Java API strongly recommends to explicitly define a version
	 * number for classes.
	 */
	private static final long serialVersionUID = -2543176471852428521L;


}
